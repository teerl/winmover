/* Copyright 2017 Thomas Lingefelt
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <libwinmove.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>

Direction str_to_dir(const char* str)
{
	if (0 == strcmp("TopLeft", str))
		return TopLeft;
	if (0 == strcmp("TopRight", str))
		return TopRight;
	if (0 == strcmp("BottomLeft", str))
		return BottomLeft;
	if (0 == strcmp("BottomRight", str))
		return BottomRight;
	return NoDir;
}

Window find_name_walk_tree(Display *disp, Window root_win, const char* find_name)
{
	Window root_return, parent_return, *children;
        Window found_child = BadWindow;
	unsigned int nchildren;
	int status = XQueryTree(disp, root_win, &root_return, &parent_return, &children, &nchildren);
	if (status == 0) {
		return BadWindow;
	}
	char *win_name = NULL;
	for (unsigned int i = 0; i < nchildren && found_child == BadWindow; ++i)
	{
		if (XFetchName(disp, children[i], &win_name))
		{
                        if (0 == strcmp(find_name, win_name)) {
                                found_child = children[i];
                        }
                        XFree(win_name);
		} else {
                    found_child = find_name_walk_tree(disp, children[i], find_name);
                }
	}
        XFree(children);
	return found_child;
}

Window find_win_id(Display *disp, const char* find_name)
{
	Window root_win = XDefaultRootWindow(disp);
	return find_name_walk_tree(disp, root_win, find_name);
}

Status move_to_corner(Display *disp, Window winid, Direction dir)
{
	if (dir == NoDir) {
		return 0;
	}
	XWindowAttributes attribs;
	XGetWindowAttributes(disp, winid, &attribs);
	int x = 0, y = 0;
	switch (dir) {
		case TopLeft:
		case BottomLeft:
			x = -attribs.x;
			break;
		case TopRight:
		case BottomRight:
			x = XDisplayWidth(disp, 0) - attribs.width - attribs.x;
		case NoDir: break;
	}
	switch (dir) {
		case TopLeft:
		case TopRight:
			y = -attribs.y;
			break;
		case BottomLeft:
		case BottomRight:
			y = XDisplayHeight(disp, 0) - attribs.height - attribs.y;
		case NoDir: break;
	}
	printf("Moving to: %dx%d\n", x, y);
	XMoveWindow(disp, winid, x, y);
	return XFlush(disp);
}

int libwinmove_default_main(int argc, const char**argv)
{
	const char *find_name = NULL;
	Direction dir = TopLeft;
	int ret = 0;
	if (argc < 2) {
		printf("Need a name of a window\n");
		return -1;
	}
	find_name = argv[1];
	if (argc >= 3) {
		dir = str_to_dir(argv[2]);
	}
	
	Display *disp = XOpenDisplay(0x0);
	Window winid = find_win_id(disp, find_name);
	if (winid == BadWindow) {
		printf("Couldn't find \"%s\"\n", find_name);
	} else {
		printf("Found window ID: 0x%lx\n", winid);
		ret = move_to_corner(disp, winid, dir);
	}
	XCloseDisplay(disp);
	return (winid && ret) ? 0 : -1;
}
