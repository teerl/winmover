#ifndef __LIBWINMOVE_H__
#define __LIBWINMOVE_H__

#include <X11/Xlib.h>

enum Direction_T {
	NoDir,
	TopLeft,
	TopRight,
	BottomLeft,
	BottomRight
};

typedef enum Direction_T Direction;

Direction str_to_dir(const char*);
Window find_name_walk_tree(Display*, Window, const char*);
Window find_win_id(Display*, const char*);
Status move_to_corner(Display*, Window, Direction);
int libwinmove_default_main(int argc, const char**argv);

#endif /* __LIBWINMOVE_H__ */
