project(winmover)
cmake_minimum_required(VERSION 2.6)
set(CMAKE_C_FLAGS "-pedantic -Wall")
find_package(X11 REQUIRED)
link_directories(${X11_X11_INCLUDE_PATH})
link_libraries(${X11_X11_LIB})

add_library(winmove
	src/libwinmove.h
	src/libwinmove.c)
include_directories("src/")
link_libraries(winmove)

add_executable(winmover src/winmover.c)
add_executable(septerra_mover src/septerra_mover.c)
