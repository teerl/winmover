# winmover

Move a window in X11 to a corner.

## Usage

```
./winmover "Title of window" <Position>
```

Position can be one of:

+ TopLeft
+ TopRight
+ BottomLeft
+ BottomRight

The first window found with the exact title will be moved to the specified corner.

## Why?

When playing an older game in wine desktop mode I've found it useful to set the resolution with

```
xrandr --output <MY OUTPUT> --mode <GAME RESOLUTION> --panning <NATIVE RESOLUTION>
```

and then move the wine desktop to one corner. `winmover` helps with automation and accuracy.

## Specialized binaries

Rather than running `winmover` with arguments a specialized binary can be build for a specific purpose. View `septerra_core.c` for an implementation of this.

## Todo

Title regex?

Add centering positions?
